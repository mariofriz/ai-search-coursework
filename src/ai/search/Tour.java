package ai.search;

import java.util.Collections;

/**
 *
 * @author Mario
 * Inspired by http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
 */
public class Tour extends Path {

    public double fitness = 0;

    public Tour() {
        for (int i = 0; i < TourManager.getNumberOfTowns(); i++) {
            this.towns.add(null);
        }
    }

    // Creates a random individual
    public void generateIndividual() {
        // Loop through all our destination cities and add them to our tour
        for (int i = 0; i < TourManager.getNumberOfTowns(); i++) {
            this.setTown(i, TourManager.getTown(i));
        }
        
        // Remove the start town
        towns.remove(TourManager.startTown);

        // Randomly reorder the tour
        Collections.shuffle(towns);
        
        // Add the start town at the beggining
        towns.add(0, TourManager.startTown);
        
        // Add goal town at the end
        towns.add(TourManager.goalTown);
    }

    public String getTown(int tourPosition) {
        return this.towns.get(tourPosition);
    }

    public void setTown(int tourPosition, String town) {
        this.towns.set(tourPosition, town);
        // If the tours been altered we need to reset the fitness and distance
        this.fitness = 0;
        this.distance = 0;
    }

    public int getDistance() {
        this.distance = 0;
        for (int i = 0; i < this.tourSize() - 1; i++) {
            this.distance += TourManager.distanceFromTo(this.getTown(i), this.getTown(i + 1));
        }
        // Add the distance from last town back to startTown
        //tourDistance += TourManager.distanceFromTo(this.getTown(this.tourSize() - 1), TourManager.startTown);
        return this.distance;
    }

    public double getFitness() {
        if (fitness == 0) {
            fitness = 1 / (double) getDistance();
        }
        return fitness;
    }

    public int tourSize() {
        return this.towns.size();
    }

    @Override
    public String toString() {
        String pathString = "Distance: " + this.distance + "\n";
        pathString += "Towns: " + this.towns;
        return pathString;
    }
}
