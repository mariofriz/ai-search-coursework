
package ai.search;

/**
 *
 * @author Mario
 */
public class AiSearch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TourManager tm = new TourManager();
        tm.addRoad(new Road("Aberdeen", "Ayr", 179, true));
        tm.addRoad(new Road("Aberdeen", "Edinburgh", 129, true));
        tm.addRoad(new Road("Aberdeen", "Fort William", 157, true));
        tm.addRoad(new Road("Aberdeen", "Glasgow", 146, true));
        tm.addRoad(new Road("Aberdeen", "Inverness", 105, true));
        tm.addRoad(new Road("Aberdeen", "St Andrews", 79, true));
        tm.addRoad(new Road("Aberdeen", "Stirling", 119, true));
        
        tm.addRoad(new Road("Ayr", "Edinburgh", 79, true));
        tm.addRoad(new Road("Ayr", "Fort William", 141, true));
        tm.addRoad(new Road("Ayr", "Glasgow", 33, true));
        tm.addRoad(new Road("Ayr", "Inverness", 207, true));
        tm.addRoad(new Road("Ayr", "St Andrews", 118, true));
        tm.addRoad(new Road("Ayr", "Stirling", 64, true));
        
        tm.addRoad(new Road("Edinburgh", "Fort William", 131, true));
        tm.addRoad(new Road("Edinburgh", "Glasgow", 43, true));
        tm.addRoad(new Road("Edinburgh", "Inverness", 154, true));
        tm.addRoad(new Road("Edinburgh", "St Andrews", 50, true));
        tm.addRoad(new Road("Edinburgh", "Stirling", 36, true));
        
        tm.addRoad(new Road("Fort William", "Glasgow", 116, true));
        tm.addRoad(new Road("Fort William", "Inverness", 74, false));
        tm.addRoad(new Road("Fort William", "St Andrews", 134, true));
        tm.addRoad(new Road("Fort William", "Stirling", 96, true));
        
        tm.addRoad(new Road("Glasgow", "Inverness", 175, true));
        tm.addRoad(new Road("Glasgow", "St Andrews", 81, true));
        tm.addRoad(new Road("Glasgow", "Stirling", 27, true));
        
        tm.addRoad(new Road("Inverness", "Fort William", 64, false));
        tm.addRoad(new Road("Inverness", "St Andrews", 145, true));
        tm.addRoad(new Road("Inverness", "Stirling", 143, true));
        
        tm.addRoad(new Road("St Andrews", "Stirling", 52, true));
        
        tm.setStartTown("Edinburgh");
        tm.setGoalTown("Edinburgh");
        
        tm.conventionalSearch();
        tm.geneticSearch();
    }
}
