package ai.search;

import java.util.ArrayList;

/**
 *
 * @author Mario
 */
public class Path implements Cloneable {

    public ArrayList<String> towns;
    public ArrayList<Road> roads;
    public int distance;

    public Path() {
        this.roads = new ArrayList<>();
        this.towns = new ArrayList<>();
        this.distance = 0;
    }

    public Path(ArrayList<Road> roads) {
        this.roads = roads;
        this.towns = new ArrayList<>();
        this.distance = 0;
    }

    public Path(ArrayList<Road> roads, ArrayList<String> towns, int distance) {
        this.roads = roads;
        this.towns = towns;
        this.distance = distance;
    }

    public int getDistance() {
        this.distance = 0;
        for (Road r : this.roads) {
            this.distance += r.getDistance();
        }
        return this.distance;
    }

    public static Path getShortestPath(ArrayList<Path> paths) {
        System.out.println("Comparing paths to find the shortest one...");
        Path shortest = paths.remove(0);
        for (Path p : paths) {
            if (p.getDistance() < shortest.getDistance()) {
                shortest = p;
            }
        }
        return shortest;
    }

    public String toString() {
        String pathString = "Distance: " + this.distance + "\n";
        pathString += "Towns: " + this.towns;
        return pathString;
    }
    
    public Path clone() {
        return new Path((ArrayList<Road>)this.roads.clone(), (ArrayList<String>)this.towns.clone(), 0);
    }
}
