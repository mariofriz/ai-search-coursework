package ai.search;

/**
 *
 * @author Mario
 */
public class Road {
    
    public String town1;
    public String town2;
    public int distance;
    public boolean bothWays;
    
    public Road(String town1, String town2, int distance, boolean bothWays) {
        this.town1 = town1;
        this.town2 = town2;
        this.distance = distance;
        this.bothWays = bothWays;
    }
    
    public int getDistance() {
        return this.distance;
    }
    
    public boolean isFrom(String town) {
        if (this.bothWays) {
            return (this.town1.equals(town) || this.town2.equals(town));
        }
        return this.town1.equals(town);
    }
    
    public Road reverse() {
        return new Road(this.town2, this.town1, this.distance, this.bothWays);
    }
    
    public Road getFrom(String town) {
        if (town.equals(this.town1)) {
            return this;
        }
        return this.reverse();
    }
    
    public String toString() {
        return this.town1 + " -> " + this.town2 + "\n";
    }
}
