package ai.search;

/**
 *
 * @author Mario
 */
import java.util.ArrayList;

public class TourManager {

    public static ArrayList<String> towns;
    public static ArrayList<Road> roads;
    private ArrayList<Path> paths;
    public static String startTown;
    public static String goalTown;

    public TourManager() {
        TourManager.roads = new ArrayList<>();
        this.paths = new ArrayList<>();
        TourManager.towns = new ArrayList<>();
    }

    public void setStartTown(String startTown) {
        TourManager.startTown = startTown;
    }

    public void setGoalTown(String goalTown) {
        TourManager.goalTown = goalTown;
    }

    public static int getNumberOfTowns() {
        return TourManager.towns.size();
    }

    public static String getTown(int index) {
        return (String) TourManager.towns.get(index);
    }

    public static int distanceFromTo(String fromTown, String destinationTown) {
        for (Road r : TourManager.roads) {
            if (r.isFrom(fromTown)) {
                Road r2 = r.getFrom(fromTown);
                if (r2.town2.equals(destinationTown)) {
                    return r.getDistance();
                }
            }
        }
        return 0;
    }

    public void addRoad(Road road) {
        TourManager.roads.add(road);
        if (!TourManager.towns.contains(road.town1)) {
            TourManager.towns.add(road.town1);
        }
        if (!TourManager.towns.contains(road.town2)) {
            TourManager.towns.add(road.town2);
        }
    }

    /**
     * Returns the available roads from a certain town
     *
     * @param town
     * @return
     */
    public ArrayList<String> getTownsSuccessors(String town) {
        ArrayList<String> successorTowns = new ArrayList<>();
        for (Road r : TourManager.roads) {
            if (r.isFrom(town)) {
                successorTowns.add(r.getFrom(town).town2);
            }
        }
        return successorTowns;
    }

    public ArrayList<Road> getRoadsFromTown(String town) {
        ArrayList<Road> roadsFrom = new ArrayList<>();
        for (Road r : TourManager.roads) {
            if (r.isFrom(town)) {
                roadsFrom.add(r.getFrom(town));
            }
        }
        return roadsFrom;
    }

    public ArrayList<Path> findAllPaths() {
        System.out.println("Finding all paths...");
        ArrayList<Path> open = new ArrayList<>();
        Path path = new Path();
        path.towns.add(TourManager.startTown);
        open.add(path);

        while (open.size() > 0) {
            path = open.remove(0);
            String parent = path.towns.get(path.towns.size() - 1);
            if (path.towns.size() > TourManager.getNumberOfTowns() && parent.equals(TourManager.goalTown)) {
                //System.out.println("ROUTE: " + path.towns);
                this.paths.add(path);
            }
            ArrayList<Road> successors = this.getRoadsFromTown(parent);
            for (int i = 0; i < successors.size(); i++) {
                Road child = successors.get(i);
                if (!path.towns.contains(child.town2) || (path.towns.size() == TourManager.getNumberOfTowns() && child.town2.equals(TourManager.goalTown))) {
                    Path newPath = path.clone();
                    newPath.towns.add(child.town2);
                    newPath.roads.add(child);
                    open.add(newPath);
                }
            }
        }
        return this.paths;
    }

    public void conventionalSearch() {
        System.out.println("--- Conventional search ---");
        long startTime = System.currentTimeMillis();
        ArrayList<Path> paths = this.findAllPaths();
        long endTime = System.currentTimeMillis();
        Path shortest = Path.getShortestPath(paths);
        System.out.println(shortest);        
        System.out.println("That took " + (endTime - startTime) + " milliseconds");
        System.out.println("---------------------------\n");
    }

    public void geneticSearch() {
        System.out.println("--- Genetic search ---");
        // Initialize population
        Population pop = new Population(50, true);
        System.out.println("Initial distance: " + pop.getFittest().getDistance());
        System.out.println(pop.getFittest());

        System.out.println("Evolving populations...");
        long startTime = System.currentTimeMillis();
        // Evolve population for 100 generations
        pop = GA.evolvePopulation(pop);
        for (int i = 0; i < 100; i++) {
            pop = GA.evolvePopulation(pop);
        }
        long endTime = System.currentTimeMillis();
        System.out.println(pop.getFittest());
        System.out.println("That took " + (endTime - startTime) + " milliseconds");
        System.out.println("------------------------\n");
    }
}
